#include <iostream>

#include "opencv2/imgcodecs.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/videoio.hpp"
#include <opencv2/highgui.hpp>
#include <opencv2/video.hpp>
#include<opencv2/photo.hpp>
#include<opencv2/features2d.hpp>

#include<vector>


using namespace cv;
using namespace std;
/*

int main()
{
    VideoCapture cap(0);


    SimpleBlobDetector::Params p;
    p.filterByArea=true;
    p.filterByCircularity=false;
    p.filterByConvexity=false;
    p.filterByInertia=false;
    p.filterByColor=true;
    p.blobColor=255;
    p.minArea=10;
    p.maxArea=1000;
    p.minDistBetweenBlobs=125;
    Ptr<SimpleBlobDetector> bd=SimpleBlobDetector::create(p);

    if(!cap.isOpened()) {cout<<"Video not found\n";exit(1);}
    Mat frame,op,op_after_filter;
    int h;
    namedWindow("after filter");
    createTrackbar("filter level","after filter",&h,100);
    Ptr<BackgroundSubtractor> bs=createBackgroundSubtractorMOG2(10);
    vector<vector<Point> > c;
    Mat im_with_kp;
    int x=0;
    vector<KeyPoint> kp;
    while(1)
    {
        cap.read(frame);
        if(frame.empty()) break;
        imshow("input",frame);
        bs->apply(frame,op);
        //imshow("output",op);
        fastNlMeansDenoising(op,op_after_filter,h);
        imshow("after filter",op_after_filter);

        /*findContours(op_after_filter,c,hier,CV_RETR_EXTERNAL,CV_CHAIN_APPROX_NONE);
        drawContours(frame,c,-1,Scalar(0,0,255));
        imshow("contours",frame);
        cout<<c.size()<<" ";

        bd->detect(op_after_filter,kp);
        cout<<kp.size()<<" ";
        drawKeypoints(op_after_filter,kp,im_with_kp,Scalar(0,0,255),DrawMatchesFlags::DRAW_RICH_KEYPOINTS);
        imshow("blob",im_with_kp);
        if(waitKey(5)=='e') break;
        ++x;
    }
    cap.release();
    return 0;
}*/

