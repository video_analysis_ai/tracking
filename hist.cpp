#include<opencv2/highgui.hpp>
#include<opencv2/imgproc.hpp>
#include<iostream>
using namespace std;
using namespace cv;

void histogram(const Mat &f,Mat &fhist)
{
    int r_bins=51,b_bins=51,g_bins=51;
    int channels[]={0,1,2};
    int h_size[]={r_bins,b_bins,g_bins};
    float range[]={0,256};
    const float *ranges[]={range,range,range};

    calcHist(&f,1,channels,Mat(),fhist,2,h_size,ranges);
    //normalize(fhist,fhist,0,1,NORM_MINMAX);
}

double hdiff(const Mat &f,const Mat &n)
{
    Mat fhsv,nhsv;
   // cvtColor(f,fhsv,COLOR_BGR2HSV);
    //cvtColor(n,nhsv,COLOR_BGR2HSV);
    /*int h_bins=90,s_bins=128;//50 & 60
   // int h_size[]={h_bins,s_bins};
    int channels[]={0,1};
    float hrange[]={0,180};
    float srange[]={0,256};
    const float *ranges[]={hrange,srange};*/

    Mat fhist,nhist;
    histogram(f,fhist);
    histogram(n,nhist);

    return compareHist(fhist,nhist,CV_COMP_BHATTACHARYYA);
}

