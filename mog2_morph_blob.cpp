#include <iostream>

#include "opencv2/imgcodecs.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/videoio.hpp"
#include <opencv2/highgui.hpp>
#include <opencv2/video.hpp>
#include<opencv2/photo.hpp>
#include<opencv2/features2d.hpp>

#include<vector>
#include<string>

#include "hist.h"
#include "obj.h"
#include "tracker.h"


using namespace cv;
using namespace std;
const int BLOB_MIN_AREA=100;//100
const int BLOB_MAX_AREA=20000;//20000
const int HISTORY=200;
const double HIST_DIFF_THRESH=0.7;
const int ORB_PARAM=8;

Ptr<ORB> od;
int frame_num=0,obj_count=0;

void find_blob(const Mat&img_after_bgs,const KeyPoint&k,Mat &op)
{
    int ffmask=8+(255<<8)+FLOODFILL_FIXED_RANGE+FLOODFILL_MASK_ONLY;

    op.create(img_after_bgs.rows+2,img_after_bgs.cols+2,CV_8UC1);
    op=Scalar(0);

    Rect r;
    floodFill(img_after_bgs,op,k.pt,Scalar(255),&r,Scalar(0),Scalar(0),ffmask);
}

void find_object(const Mat &bsmask_thresh,const Mat &input,const KeyPoint &p,Mat &output)
{
        Mat mask;
        mask.create(bsmask_thresh.rows+2,bsmask_thresh.cols+2,CV_8UC1);
        mask=Scalar(0);
        Rect r;
        int ffmask=8+(255<<8)+FLOODFILL_FIXED_RANGE+FLOODFILL_MASK_ONLY;
        floodFill(bsmask_thresh,mask,p.pt,Scalar(255),&r,Scalar(0),Scalar(0),ffmask);
        //rectangle(mask,r,Scalar(255),2);
        imshow("ffmask",mask);
        //if(r.width>0 && r.height>0){
        mask=mask(r);
        //imshow("cropped mask",mask);


        Mat channel[3];
        split(input,channel);

        channel[0]=channel[0](r);
        channel[1]=channel[1](r);
        channel[2]=channel[2](r);
        bitwise_and(channel[0],mask,channel[0]);
        bitwise_and(channel[1],mask,channel[1]);
        bitwise_and(channel[2],mask,channel[2]);
        //y=z;
        merge(channel,3,output);
}

int match_hist(const vector<obj> &v,const Mat &h)
{
    double min_diff=INT_MAX;
    int found_obj=-1;
    for(int i=0;i<v.size();++i)
    {
        double d=compareHist(v[i].mrh,h,CV_COMP_BHATTACHARYYA);
        if(d<HIST_DIFF_THRESH)
        {
            if(d<min_diff)
            {
                min_diff=d;
                found_obj=i;
            }
        }
    }
    return found_obj;
}

void update_mvs_area(const Mat &img_after_bgs,vector<obj> &obj_list,const vector<KeyPoint> &k)
{
    vector<Mat> blobs;
    bool * blob_matched=new bool[k.size()];
    for(int i=0;i<k.size();++i)
    {
        Mat dummy;
        find_blob(img_after_bgs,k[i],dummy);
        blobs.push_back(dummy);
        blob_matched[i]=false;
    }
    for(int i=0;i<obj_list.size();++i)
    {
        obj_list[i].matched=false;
    }
    for(int i=0;i<k.size();++i)
    {
        bool f=false;
        int diff=INT_MAX,z=-1;
        for(int j=0;j<obj_list.size();++j)
        {
            if(obj_list[j].status!=DELETED&&obj_list[j].isCloseMatch(k[i].pt)){
                //v[j].addPoint(k[i].pt);
                if(obj::dist(k[i].pt,obj_list[j].mrp)<diff)
                {
                    diff=obj::dist(k[i].pt,obj_list[j].mrp);
                    z=j;
                }
                f=true;

            }
        }
        if(f&&obj_list[z].status!=DELETED)
        {
            if(aoi(obj_list[z].mrb,blobs[i])>0){
                obj_list[z].addPoint(k[i].pt);
                obj_list[z].mrb=blobs[i];
                obj_list[z].matched=true;
                blob_matched[i]=true;
            }
        }
    }
    /*for(int i=0;i<obj_list.size();++i)
    {
        for(int j=0;j<blobs.size();++j)
        {
            if(aoi(obj_list[i].mrb,blobs[j])>0)
            {
                obj_list[i].addPoint(k[j].pt);
                obj_list[i].mrb=blobs[j];
                blob_matched[j]=true;
                break;
            }
        }
    }*/
    for(int i=0;i<k.size();++i)
    {
        if(!blob_matched[i])
        {
            obj x;
            x.mrb=blobs[i];
            x.addPoint(k[i].pt);
            obj_list.push_back(x);
        }
    }
    update_status(obj_list);
    delete blob_matched;
}
/*
void update_mvs_his(vector<obj> &v,const vector<KeyPoint> &k,const Mat &input,const Mat &bsthresh)
{
    for(int i=0;i<k.size();++i)
    {
        bool f=false;
        int diff=INT_MAX,z=-1;
        for(int j=0;j<v.size();++j)
        {
            if(v[j].isCloseMatch(k[i].pt)){
                //v[j].addPoint(k[i].pt);
                if(obj::dist(k[i].pt,v[j].mrp)<diff)
                {
                    diff=obj::dist(k[i].pt,v[j].mrp);
                    z=j;
                }
                f=true;

            }
        }
        if(!f)
        {
            Mat y,z;
            find_object(bsthresh,input,k[i],z);
            histogram(z,y);
            int m;
            if((m=match_hist(v,y))>0)
            {
                v[m].addPoint(k[i].pt);
                v[m].mrh=y;
            }
            else{
                obj x(obj_count++);
                x.addPoint(k[i].pt);
                x.mrh=y;
                v.push_back(x);
            }
        }
        else
        {
            v[z].addPoint(k[i].pt);
            Mat x;
            find_object(bsthresh,input,k[i],x);
            histogram(x,v[z].mrh);
            //cout<<"hi";
        }
    }
}
*/
void disp_mvs(vector<obj> &v,Mat &f)
{
    for(int i=0;i<v.size();++i)
    {
        v[i].outputMv(f);
    }
}

void disp_ids(vector<obj> &v,Mat &f)
{
    for(int i=0;i<v.size();++i)
    {
        v[i].outputId(f);
    }
}

double maximum(double x,double y)
{
    return x>y?x:y;
}

void filter_matches(const vector<DMatch>&m ,vector<DMatch>& good_matches)
{
    good_matches.clear();
    double mind=100;
    for(int i=0;i<m.size();++i)
    {
        if(m[i].distance<mind)mind=m[i].distance;
    }
    cout<<"min dist="<<mind<<endl;

    for(int i=0;i<m.size();++i)
    {
        if(m[i].distance<maximum(0.02,1.2*mind))
            good_matches.push_back(m[i]);
    }
}


int main(int argc,char*argv[])
{
    VideoCapture c;

    vector<obj> objs;

    od=ORB::create(500,1.2f,8,ORB_PARAM,0,2,ORB::HARRIS_SCORE,ORB_PARAM);
    BFMatcher bm(NORM_HAMMING,true);
    Mat matches;

    Mat yg,zg;

    SimpleBlobDetector::Params p;
    p.filterByArea=true;
    p.filterByCircularity=false;
    p.filterByConvexity=false;
    p.filterByInertia=false;
    p.filterByColor=true;
    p.blobColor=255;
    p.minArea=BLOB_MIN_AREA;
    p.maxArea=BLOB_MAX_AREA;

   // p.minDistBetweenBlobs=30; //not required for video003

    Ptr<SimpleBlobDetector> bd=SimpleBlobDetector::create(p);
    vector<KeyPoint> kp,ky,kz;

    vector<DMatch> matchyz,good_matches;
    Mat dy,dz;

    Mat se1=getStructuringElement(MORPH_RECT,Size_<int>(5,5)),f,bsmask,im_with_kp;
    Mat se2=getStructuringElement(MORPH_RECT,Size_<int>(2,2));
    Mat mask,y,z;
    Ptr<BackgroundSubtractor> bs=createBackgroundSubtractorMOG2(HISTORY);
    /*const string src="D:\\vijai_files\\test\\test\\bin\\Debug\\";
    c.open((src+string(argv[1])).c_str());*/
    c.open(string(argv[1]).c_str());
    if(!c.isOpened()) cout<<"Error opening\n";
    cvNamedWindow("blob",CV_WINDOW_NORMAL);
    cvSetWindowProperty("blob", CV_WND_PROP_FULLSCREEN, CV_WINDOW_FULLSCREEN);

    bool flag=false;
    KeyPoint pp;
    Mat pf;

/*To be used only for per_breach video*/
        /*c.read(f);
        ++frame_num;
        imshow("input",f);
        Mat t=f;
        bs->apply(f,bsmask);
        /************/


    while(1)
    {
        if(!c.read(f))break;
        ++frame_num;
        if(waitKey(1)=='e')break;
        //imshow("input",f);
        Mat t=f;
        bs->apply(f,bsmask);
        //imshow("bsmask",bsmask);
        /*fastNlMeansDenoising(bsmask,nlop,65);
        imshow("nlop",nlop);*/
        morphologyEx(bsmask,bsmask,MORPH_OPEN,se2);
       // imshow("bsmask_after_open",bsmask);
        morphologyEx(bsmask,bsmask,MORPH_CLOSE,se1);
        //imshow("bs_after_open_close",bsmask);

        bd->detect(bsmask,kp);


        Mat bsmask_thresh;
        threshold(bsmask,bsmask_thresh,1,255,0);
        imshow("bsthresh",bsmask_thresh);

        /*if(kp.size()>0){
                if(!flag){flag=true;
                    pp=kp[0];
                    pf=bsmask_thresh;
                }
                else{
                    Mat m1,m2;
                    find_blob(pf,pp,m1);
                    find_blob(bsmask_thresh,kp[0],m2);
                    cout<<"aoi="<<aoi(m1,m2)<<endl;
                    pp=kp[0];
                    pf=bsmask_thresh;
                }
                /*
        y=z;
        find_object(bsmask_thresh,f,kp[0],z);
        if(!y.empty())
        {
            destroyWindow("y");
            //destroyWindow("z");
            imshow("y",y);
            //imshow("z",z);
            //double d=hdiff(z,y);
            //cout<<"diff="<<d<<"\t";
            cvtColor(y,yg,CV_BGR2GRAY);
            cvtColor(z,zg,CV_BGR2GRAY);

            od->detect(yg,ky);
            od->detect(zg,kz);

            od->compute(yg,ky,dy);
            od->compute(zg,kz,dz);

            if(!dy.empty()&&!dz.empty()){
                 cout<<endl<<endl;

                /*cout<<"0 with 0\n";
                destroyWindow("matches");
                bm.match(dy,dz,matchyz);
                filter_matches(matchyz,good_matches);
                cout<<"good_matches="<<good_matches.size()<<endl;
                drawMatches(yg,ky,zg,kz,good_matches,matches,Scalar::all(-1),Scalar::all(-1),vector<char>(),DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);
                imshow("matches",matches);

                for(int p=0;p<kp.size();++p){
                    cout<<"compare with "<<p<<endl;

                    char ap[5];
                    itoa(p,ap,10);
                    destroyWindow((const char *)ap+(const string&)"matches");
                    find_object(bsmask_thresh,f,kp[p],z);
                    cvtColor(z,zg,CV_BGR2GRAY);
                    od->detect(zg,kz);
                    od->compute(zg,kz,dz);
                    bm.match(dy,dz,matchyz);
                    filter_matches(matchyz,good_matches);
                    cout<<"good_matches="<<good_matches.size()<<endl;
                    drawMatches(yg,ky,zg,kz,good_matches,matches,Scalar::all(-1),Scalar::all(-1),vector<char>(),DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);
                    imshow((const char *)ap+(const string&)"matches",matches);
                }
                for(int p=0;p<kp.size();++p)
                {
                    stringstream s;
                    s<<"kp["<<p<<"]";
                    putText(t,s.str(),kp[p].pt,CV_FONT_HERSHEY_SIMPLEX,0.5,Scalar(255,0,0));
                }
            }
        }

        }*/
       // update_mvs_his(objs,kp,f,bsmask_thresh);

        update_mvs_area(bsmask_thresh,objs,kp);

        disp_mvs(objs,t);
        disp_ids(objs,t);

        cout<<kp.size()<<" ";
        //drawKeypoints(bsmask,kp,t,Scalar(0,0,255),DrawMatchesFlags::DRAW_OVER_OUTIMG);
        imshow("blob",t);
/*
        IplImage x=t;
        cvShowImage("blob",&x);
*/
        //if(waitKey(5)=='e') break;
    }
    output_mvs(objs);
}
