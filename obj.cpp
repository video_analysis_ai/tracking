#include<iostream>
#include "obj.h"
#include "mog2_morph_blob.h"

const int THRESH_DIFF=100;//(prev=20)how close should the next detected point be from the current location of an object to be treated as its next point
int obj::THRESH=THRESH_DIFF;

float obj::dist(const Point&p1,const Point &p2)
{
        Point p=p1-p2;
        return sqrt(p.x*p.x+p.y*p.y);
}

obj::obj(){
        status=HYPOTHESIS;
        matched=true;
        frame_cnt=0;
    }
bool obj::isCloseMatch(const Point &p){
        return dist(p,mrp)<=THRESH;
    }
void obj::addPoint(const Point &p){
        mv.push_back(p);
        mrp=p;
    }

bool obj::valid()
{
    return status==NORMAL;
}
void obj::outputMv(Mat &f){
    if(valid()){
        vector<Point> smoothened_mv;
        approxPolyDP(mv,smoothened_mv,3,false);
        for(int i=0;i<smoothened_mv.size()-1;++i)
        {
            line(f,smoothened_mv[i],smoothened_mv[i+1],Scalar(255,255,255),1);
        }
    }
}
void obj::outputMv_test(Mat&f)
{
    for(int i=0;i<mv.size()-1;++i)
        {
            line(f,mv[i],mv[i+1],Scalar(255,255,255),1);
        }
}
void obj::outputId(Mat &f){
    if(valid()){
        stringstream s;
        s<<obid;
        putText(f,s.str(),mrp,CV_FONT_HERSHEY_SIMPLEX,0.5,Scalar(0,0,255));
        int ffmask=8+(255<<8)+FLOODFILL_FIXED_RANGE+FLOODFILL_MASK_ONLY;
        Mat op;
        op.create(mrb.rows+2,mrb.cols+2,CV_8UC1);
        op=Scalar(0);

        Rect r;
        floodFill(mrb,op,mrp,Scalar(255),&r,Scalar(0),Scalar(0),ffmask);
        rectangle(f,r,Scalar(255,0,0),2);
        }
}
int obj::op_mv()
{
    if(mv.size()>MIN_NUM_OF_POINTS_VALID_MV){
        std::cout<<endl<<"oid="<<obid<<endl;
        for(int i=0;i<mv.size();++i)
            std::cout<<mv[i].x<<","<<mv[i].y<<","<<"  ";
        std::cout<<endl<<endl;
        return 1;
    }
    return 0;
}
