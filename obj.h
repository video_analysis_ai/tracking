#ifndef obj_h
#define obj_h
#include<opencv2/imgproc/imgproc.hpp>
//#include "tracker.h"
using namespace cv;
using namespace std;

const int MIN_NUM_OF_POINTS_VALID_MV=160;

enum object_state{NORMAL,HYPOTHESIS,LOST,DELETED};

class obj{
    vector<Point> mv;

    bool valid();

public:

    Point mrp;
    Mat mrh;
    Mat mrd;
    object_state status;
    bool matched;
    int frame_cnt;
    int obid;

    Mat mrb;//for area comparison; can be removed later

    obj();

    bool isCloseMatch(const Point &p);
    void addPoint(const Point &p);
    void outputMv(Mat &f);
    void outputId(Mat &f);
    int op_mv();
    void outputMv_test(Mat&);


    static float dist(const Point&p1,const Point &p2);

    static int THRESH;
};

#endif
