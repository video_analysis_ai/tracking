#include <opencv2/core/utility.hpp>
#include <iostream>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
#include<opencv2/video/tracking.hpp>

#define BINARY 0


using namespace cv;
using namespace std;

VideoCapture cap(0);
Mat bframe;
int thresh;
KalmanFilter kf(2,2);

void getbckframe(Mat &bframe)
{
    while(1)
    {
        cap>>bframe;
        imshow("ip",bframe);
        char c=waitKey(2);
        if(c=='b'){
            break;
        }
    }

}
void get_thresh_image(Mat &bframe,Mat &frame,Mat &res)
{
    absdiff(bframe,frame,res);
    cvtColor(res,res,CV_RGB2GRAY);
    threshold(res,res,thresh,255,BINARY);
}
Point calcmom(Mat &im)
{
    Moments m=moments(im);
    Point p(m.m10/m.m00,m.m01/m.m00);
    return p;
}
void init(KalmanFilter &kf,Mat &bframe)
{
    kf.transitionMatrix=(Mat_<float>(2,2)<<1,0, 0,1);
    setIdentity(kf.measurementMatrix);
    kf.processNoiseCov.setTo(Scalar(1e-4));
    setIdentity(kf.measurementNoiseCov,Scalar(0));
    setIdentity(kf.errorCovPost,Scalar(0.1));
    Mat frame,diff,gdiff;
    namedWindow("op");
    createTrackbar("threshold","op",&thresh,256);
    while(1)
    {
        cap>>frame;
        get_thresh_image(bframe,frame,gdiff);
        imshow("op",gdiff);
       char c=waitKey(10);
        if(c=='i')
            break;
    }
    cout<<thresh<<endl;
    /*imshow("init",frame);
        waitKey(0);
    Mat diff,gdiff;
    absdiff(bframe,frame,diff);
    imshow("diff",diff);
        waitKey(0);

    cvtColor(diff,gdiff,CV_RGB2GRAY);
    imshow("gdiff",gdiff);
        waitKey(0);
    threshold(gdiff,gdiff,THRESHOLD,255,BINARY);
    imshow("bgdiff",gdiff);
    waitKey(0);*/

    Point p=calcmom(gdiff);

    kf.statePre.at<float>(0)=p.x;
    kf.statePre.at<float>(0)=p.y;
   // cout<<p.x<<" "<<p.y<<endl;

    /*Rect_<int> r;
    cout<<p.x<<" "<<p.y<<endl;
    r.x=p.x-10;
    r.y=p.y-10;
    r.width=20;
    r.height=20;
    rectangle(gdiff,r,Scalar(0,255,0),3);
    imshow("op",gdiff);
    waitKey(0);*/
}
void track()
{
    while(1)
    {
        Mat frame,tframe;
        cap>>frame;
        get_thresh_image(bframe,frame,tframe);
        Point actpoint=calcmom(tframe);
        //vector<Point> temp;
        Mat kfpre=kf.predict();
        Point kfpoint;
        //cout<<"1\n";
        kfpoint.x=kfpre.at<int>(0,0);
        //cout<<"2\n";
        kfpoint.y=kfpre.at<int>(1,0);
        Rect2d r;
        r.x=actpoint.x-10;
        r.y=actpoint.y-10;
        r.width=20;
        r.height=20;
        rectangle(frame,r,Scalar(0,255,0),3);
        r.x=kfpoint.x-10;
        r.y=kfpoint.y-10;
        cout<<kfpoint.x<<" "<<kfpoint.y<<endl;
        rectangle(frame,r,Scalar(0,0,255),3);
        //cout<<"3\n";
        Mat_<float> t(2,1);
        t(0)=actpoint.x;
        t(0)=actpoint.y;
        kf.correct(t);
        //cout<<"4\n";
        imshow("op",frame);
        char c=waitKey(5);
        if(c=='e') break;
    }
}
int main()
{
    cout<<"Hi\n";
    getbckframe(bframe);
    imshow("bgnd",bframe);
    waitKey(0);
    init(kf,bframe);
    track();
    return 1;

}
