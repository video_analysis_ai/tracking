#include "tracker.h"
#include<opencv2/core/core.hpp>
#include<opencv2/highgui/highgui.hpp>
#include<opencv2/features2d/features2d.hpp>

#include"mog2_morph_blob.h"

const int TIME_FOR_HYPO_STATE=15;
const int MAX_TIME_FOR_LOST_STATE=30;

void tracker::update(const Mat&bgf)
{

}
double find_largest_area(const vector<vector<Point> >&k)
{
    double maxarea=0;
    for(int i=0;i<k.size();++i)
    {
        if(contourArea(k[i])>maxarea) maxarea=contourArea(k[i]);
    }
    return maxarea;
}
double aoi(const Mat &blob1_mask,const Mat&blob2_mask)
{
    /*int ffmask=8+(255<<8)+FLOODFILL_FIXED_RANGE+FLOODFILL_MASK_ONLY;
    Mat mask1,mask2;
    mask1.create(img_after_bgs.rows+2,img_after_bgs.cols+2,CV_8UC1);
    mask1=Scalar(0);
    mask2.create(img_after_bgs.rows+2,img_after_bgs.cols+2,CV_8UC1);
    mask2=Scalar(0);
    Rect r;
    floodFill(img_after_bgs,mask1,c1.pt,Scalar(255),&r,Scalar(0),Scalar(0),ffmask);
    floodFill(img_after_bgs,mask2,c2.pt,Scalar(255),&r,Scalar(0),Scalar(0),ffmask);*/
    //imshow("blob1m",blob1_mask);
    //imshow("blob2m",blob2_mask);
    vector<vector<Point> > k;
    Mat dummy,inter;
    bitwise_and(blob1_mask,blob2_mask,inter);
    //imshow("inter",inter);

    findContours(inter,k,dummy,CV_RETR_EXTERNAL,CV_CHAIN_APPROX_SIMPLE);

    //drawContours(mask1,k1,-1,Scalar(100));

    if(!k.empty())
    return find_largest_area(k);
    else return 0;
}


void update_status(vector<obj>&obj_list)
{
    for(int i=0;i<obj_list.size();++i)
    {
        switch(obj_list[i].status)
        {
            case NORMAL:if(!obj_list[i].matched){
                            obj_list[i].status=LOST;
                            obj_list[i].frame_cnt=0;
                        }
                        break;
            case HYPOTHESIS:
                            if(obj_list[i].matched)
                                ++obj_list[i].frame_cnt;
                            else
                                obj_list[i].status=DELETED;
                            if(obj_list[i].frame_cnt>TIME_FOR_HYPO_STATE)
                                {
                                    obj_list[i].status=NORMAL;
                                    obj_list[i].obid=++obj_count;
                                }

                            break;
            case LOST:
                        if(!obj_list[i].matched) ++obj_list[i].frame_cnt;
                        if(obj_list[i].frame_cnt>MAX_TIME_FOR_LOST_STATE)
                            {
                                obj_list[i].status=DELETED;
                            }
                            if(obj_list[i].matched)
                                obj_list[i].status=NORMAL;
                            break;
        }
    }
}
void output_mvs(const vector<obj>&v)
{
    int s=0;
    Mat f;
    f.create(3000,3000,CV_8UC3);
    for(int i=0;i<v.size();++i)
        if(v[i].op_mv()){
                f=Scalar(0);
                v[i].outputMv_test(f);
                imshow("filtered mvs",f);
                ++s;
                waitKey(0);
        }
    std::cout<<endl<<endl<<"num of objects="<<s;
}
