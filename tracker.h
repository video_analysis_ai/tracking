#include<list>
#include<iostream>

#include "obj.h"
#include "blob.h"



extern double aoi(const Mat &,const Mat&);
extern void update_status(vector<obj>&obj_list);
extern void output_mvs(const vector<obj>&);

class tracker{
    list<obj> obj_list;
    list<blob> blob_list;

    list<blob> get_blobs(const Mat&);
public:
    void update(const Mat&);

};

